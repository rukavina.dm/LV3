# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 08:15:59 2015

@author: laptop
"""
#pyton 3.4
#rukavina.dm

import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import numpy as np
# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=1.1.2014&vrijemeDo=31.12.2014Test'
# url nakon podesavanja podataka
airQualityHR = urllib.request.urlopen(url).read()#dodan request
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

dfa=df.sort('mjerenje')#sortirani niz po mjerenju
print(dfa.vrijeme.tail(3))#posljenja tri najveca
plt.figure(2,figsize=(16,8))
for i in range(1,len(dfa)):#petlja po svim podacima
    plt.bar(dfa.vrijeme[i],dfa.mjerenje[i])#prikaz podataka po vremenu i mjerenju
#
#f=df[df['month']==2]#kod neradi
#s=df[df['month']==9] 
#dtp=[f.mjerenje,s.mjerenje.tail(28)]
#print(dtp)
##d2p=data.frame(dtp)
#plt.figure(3)
#feb=f.mjerenje
#sep=s.mjerenje
#plt.boxplot(sep)
#
#plt.boxplot(feb)

r=df[df['dayOfweek']<=4]#radni dani
v=df[df['dayOfweek']>4]#vikend dani

plt.figure(4)
data=[r.mjerenje,v.mjerenje]#spremanje podataka u listu
plt.boxplot(data)#prikaz distrubucija